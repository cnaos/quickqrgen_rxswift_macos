//
//  ViewController.swift
//  QuickQRGen_RxSwift_MacOS
//
//  Created by cnaos on 2018/09/25.
//  Copyright © 2018年 cnaos. All rights reserved.
//

import Cocoa
import RxSwift
import RxCocoa
import QRCoder

class ViewController: NSViewController {
    @IBOutlet weak var textField: NSTextField!
    @IBOutlet weak var correctionLevelSlider: NSSlider!
    @IBOutlet weak var correctionLevelLabel: NSTextField!
    @IBOutlet weak var qrImageView: NSImageView!

    let disposeBag = DisposeBag()
    let generator = QRCodeGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textField.stringValue = "Hello world!"

        correctionLevelSlider.doubleValue = 2.0
        self.setupQRLevelLabelUpdate()
        self.setupQRImageUpdate()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    private func convertSliderLevel( _ level: Int) -> QRCodeGenerator.CorrectionLevel {
        switch level {
        case 0:
            return QRCodeGenerator.CorrectionLevel.L
        case 1:
            return QRCodeGenerator.CorrectionLevel.M
        case 2:
            return QRCodeGenerator.CorrectionLevel.Q
        case 3:
            return QRCodeGenerator.CorrectionLevel.H
        default:
            return QRCodeGenerator.CorrectionLevel.Q
        }
    }
    
    private func convertQRCorrectionLevel( _ level: QRCodeGenerator.CorrectionLevel) -> String {
        return level.rawValue
    }
    
    private func setupQRLevelLabelUpdate() {
        self.correctionLevelSlider.rx.value
            .map{
                self.convertQRCorrectionLevel( self.convertSliderLevel( Int($0)))
            }
            .bind(to: self.correctionLevelLabel.rx.text)
            .disposed(by: self.disposeBag)
    }
    
    private func setupQRImageUpdate(){
        Observable
            .combineLatest(self.textField.rx.text, self.correctionLevelSlider.rx.value) {
                self.generator.correctionLevel = self.convertSliderLevel( Int($1))
                return self.generator.createImage(value: $0! ,size: CGSize(width: 200, height:200))!
            }
            .bind(to: self.qrImageView.rx.image)
            .disposed(by: self.disposeBag)
    }
}

