QuickQRGen_RxSwift_MacOS

--------------

## 概要

RxSwift, RxCocoa, QRCoder
をつかったMacOS用のQRコードジェネレータです。

テキストフィールドに入力された文字列を即座にQRコードに反映します。

RxSwiftをつかった簡単なサンプルとしてつくりました。

## スクリーンショット

![スクリーンショット](images/screenshot1.png)
